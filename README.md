# eForm Activation Status

This plugins lists the current activation status of eForm WordPress Plugin.

Install and check under eForm > Activation Status.
