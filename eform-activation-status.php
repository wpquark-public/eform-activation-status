<?php
/**
 * Plugin Name: eForm Activation Server Status
 * Description: Tests and outputs activation status and connection status for eForm
 * Plugin URI: https://eform.live
 * Author: WPQuark
 * Author URI: https://wpquark.com
 * Version: 1.0.0
 * License: GPL3
 * Text Domain: wpq-efas
 * Domain Path: translation
 */

/*
    Copyright (C) 2018  WPQuark  contact@wpquark.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

function eform_activation_menu( $init_classes ) {
	require_once dirname( __FILE__ ) . '/class-ipt-fsqm-activation-test.php';
	$init_classes[] = 'IPT_FSQM_Activation_Test';
	return $init_classes;
}
add_filter( 'ipt_fsqm_admin_menus', 'eform_activation_menu', 10, 1 );
