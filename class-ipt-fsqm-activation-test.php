<?php
/**
 * Class responsible for the Activation Status menu on eForm
 *
 * @package eForm Activation Status
 * @subpackage Admin
 * @author Swashata Ghosh <swashata@wpquark.com>
 */
class IPT_FSQM_Activation_Test extends IPT_FSQM_Admin_Base {
	/**
	 * Server URL. Change this to point to your server
	 *
	 * @var        string
	 */
	const SERVER = 'https://dist.wpquark.io';
	/**
	 * Activation endpoint. Do not change this
	 *
	 * @var        string
	 */
	const ACTIVATION_ENDPOINT = 'wpupdate/v1/activate';
	/**
	 * Plugin slug which is registered to the update server
	 *
	 * @var        string
	 */
	const SLUG = 'wp-fsqm-pro';
	/**
	 * WP Options Table key, where token would be stored
	 *
	 * @var        string
	 */
	const TOKEN_OPTIONS = 'eform_update_token';

	/**
	 * WP Options Table key, where status would be stored
	 *
	 * @var        string
	 */
	const STATUS_OPTION = 'eform_update_status';

	public function __construct() {
		$this->capability = 'manage_feedback';
		$this->action_nonce = 'ipt_fsqm_activation_test';

		parent::__construct();

		$this->icon = 'database';
	}

	public function admin_menu() {
		$this->pagehook = add_submenu_page( 'ipt_fsqm_dashboard', __( 'eForm - Activation Status', 'wpq-efas' ), __( 'Activation Status', 'wpq-efas' ), $this->capability, 'ipt_fsqm_activation_status', array( $this, 'index' ) );
		parent::admin_menu();
	}

	public function index() {
		$this->index_head( __( 'eForm <span class="ipt-icomoon-arrow-right2"></span> Activation Status', 'wpq-efas' ) . '<a href="admin.php?page=ipt_fsqm_settings" class="add-new-h2">' . __( 'Add Purchase Code', 'wpq-efas' ) . '</a>', false );

		$this->ui->iconbox( __( 'Activation Status', 'wpq-efas' ), array( $this, 'activation_status' ), 'switch' );
		$this->ui->iconbox( __( 'Connection Status', 'wpq-efas' ), array( $this, 'connection_status' ), 'link2' );

		$this->index_foot();
	}

	public function activation_status() {
		global $ipt_fsqm_settings;
		$updater = EForm_AutoUpdate::instance();
		$eform_activation_status = $updater->current_activation_status( $ipt_fsqm_settings['purchase_code'] );
		$items = [];
		$items[] = [
			'name' => '',
			'label' => __( 'Status', 'wpq-efas' ),
			'ui' => true == $eform_activation_status['activated'] ? 'msg_okay' : 'msg_error',
			'param' => array( $eform_activation_status['msg'] ),
		];
		$items[] = [
			'name' => '',
			'label' => __( 'Activation Token', 'wpq-efas' ),
			'ui' => 'msg_update',
			'param' => [ $this->get_token() ],
		];
		$items[] = [
			'name' => '',
			'label' => __( 'Purchase Code', 'wpq-efas' ),
			'ui' => 'msg_update',
			'param' => [ $ipt_fsqm_settings['purchase_code'] ],
		];
		$this->ui->form_table( $items );
	}

	public function connection_status() {
		global $ipt_fsqm_settings;
		$result = wp_remote_get( $this->get_activation_url(), array(
			'body' => array(
				'purchase_code' => $ipt_fsqm_settings['purchase_code'],
				'domain' => $this->get_domain(),
			),
		) );
		if ( is_wp_error( $result ) ) {
			$this->ui->msg_error( __( 'Could not connect to activation server.', 'wpq-efas' ) . ' ' . $result->get_error_message() );
		} else {
			if ( 200 != $result['response']['code'] ) {
				$this->ui->msg_error( __( 'Invalid Response Code from Update Server. Error Message: ', 'wpq-efas' ) . $result['response']['code'] . ' | ' . $result['response']['message'] );
			} else {
				$this->ui->msg_okay( __( 'Successfully connected to update server.', 'wpq-efas' ) );
			}
			$json = json_decode( $result['body'], true );
			if ( ! $json ) {
				$this->ui->msg_error( __( 'Invalid Response Body from Update Server.', 'wpq-efas' ) );
				echo '<pre>' . $result['body'] . '</pre>';
			} else {
				$items = [];
				foreach ( $json as $key => $val ) {
					$items[] = [
						'name' => '',
						'label' => $key,
						'ui' => 'msg_update',
						'param' => [ print_r( $val, true ) ],
					];
				}
				$this->ui->form_table( $items );
			}
		}
		echo '<h3>' . __( 'Raw Response from Server', 'wpq-efas' ) . '</h3>';
		echo '<pre>' . print_r( $result, true ) . '</pre>';
	}

	/**
	 * Gets the activation url.
	 *
	 * @return     string  The activation url.
	 */
	protected function get_activation_url() {
		return self::SERVER . '/wp-json/' . self::ACTIVATION_ENDPOINT . '/' . self::SLUG;
	}

	/**
	 * Gets the domain of this WordPress installation
	 *
	 * @return     string  The domain.
	 */
	protected function get_domain() {
		return $_SERVER['HTTP_HOST'];
	}

	/**
	 * Gets the token from WP Options Table
	 *
	 * @return     string  The token.
	 */
	protected function get_token() {
		return get_option( self::TOKEN_OPTIONS, '' );
	}
}
